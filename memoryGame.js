class Game { 
    constructor() {
      this.result = "";
    }
    getResult() {
      return this.result;
    }
    setup (id) {
      // Your code goes here
    let myTable = document.querySelector(`#${id}`);
    let allTds = myTable.querySelectorAll("td");
    allTds.forEach(item => {
        item.addEventListener('click', event => {
          let word = event.target.innerText
          if(!this.result.includes(word))
          this.result += word
        })
      })
    }
  }

  document.body.innerHTML = `
  <table id="game-matrix">
    <tbody>
      <tr><td>A</td><td>J</td></tr>
      <tr><td>A</td><td>B</td></tr>
    </tbody>
  </table>`;
  
  let gameObj = new Game();
  gameObj.setup("game-matrix");
  document.getElementsByTagName('td')[0].click();
  document.getElementsByTagName('td')[3].click();
  document.getElementsByTagName('td')[1].click();
  document.getElementsByTagName('td')[2].click();
  console.log(gameObj.result); 
  
  