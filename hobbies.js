function findAllHobbyists(hobby, hobbies) {
    let nameArr = []
    for (let key in hobbies) {
        if (hobbies[key].includes(hobby)) {
            nameArr.push(key);  
        }
    }
    return nameArr;
  }
  
  var hobbies = {
    "Steve": ['Fashion', 'Piano', 'Reading'],
    "Patty": ['Drama', 'Magic', 'Pets'],
    "Chad": ['Puzzles', 'Pets', 'Yoga']
  };
  
  console.log(findAllHobbyists('Yoga', hobbies));
  