class Snapshot {
  constructor(array) {
    this.array = Array.from(array);
  }
  
  restore = () => {
    return Array.from(this.array);  
  }
}

var array = [1, 2];
var snap = new Snapshot(array);
array[0] = 3;
console.log(array);
array = snap.restore();
console.log(array.join()); //It should log "1,2"
array.push(4);
array = snap.restore();
console.log(array.join()); //It should log "1,2"
