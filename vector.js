function findShortest(vectors) {
    // Write the code that goes 
    let lengthValue = [];
    for(let i=0; i < vectors.length; i++){
        lengthValue.push(Math.abs(vectors[i][0]) + Math.abs(vectors[i][1]) + Math.abs(vectors[i][2]))
    }
    const min = Math.min(...lengthValue);
    return vectors[lengthValue.indexOf(min)]
  }
  
  var vectors = [[1, 1, 1], [2, 2, 2], [3, 3, 3]];
  var shortest = findShortest(vectors);
  console.log(shortest);
  