function numberOfItems(arr, item) {
    // Write the code that goes here
    let count = 0;
    for(let i=0; i < arr.length; i++){
        if(Array.isArray(arr[i]))
        count += numberOfItems(arr[i], item)
        
        else if(arr[i]==item )
        count++
    }
    return count
  }
  
  var arr = [
    25,
    "apple",
    ["banana", 25,"strawberry", "apple", 25]
  ];
  console.log(numberOfItems(arr, 25));
  console.log(numberOfItems(arr, "apple"));
  