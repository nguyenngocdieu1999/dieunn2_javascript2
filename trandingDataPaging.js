function getPageData(dayTrade, pageSize, pageNumber) {
    // Your code goes here
    let data = JSON.parse(dayTrade)

    let key = 'user';
    let value = "countOfStocks";
    let getKey = o => key.map(k => o[k]).join('|')
    let getObject = o => Object.fromEntries([...key.map(k => [k, o[k]]), [value, 0]]);

    key = [].concat(key);

    let newData = Object.values(data.reduce((r, o) => {
        (r[getKey(o)] ??= getObject(o))[value] += +o[value];
        return r;
    }, {}));
    
    newData.sort((a,b) => b.countOfStocks - a.countOfStocks)
    console.log(newData);
    
    return newData.slice(pageSize*pageNumber-pageSize, pageSize*pageNumber)
  }

  var dayTrade = 
  `[
    {"user": "Rob", "company": "Google", "countOfStocks": 5},
    {"user": "Bill", "company": "Goldman", "countOfStocks": 18},
    {"user": "Rob", "company": "JPMorgan", "countOfStocks": 10},
    {"user": "Dave", "company": "Boeing", "countOfStocks": 10},
    {"user": "Miley", "company": "Microsoft", "countOfStocks": 12},
    {"user": "Dieu", "company": "Microsoft", "countOfStocks": 14},
    {"user": "abc", "company": "Microsoft", "countOfStocks": 12},
    {"user": "b", "company": "Microsoft", "countOfStocks": 12},
    {"user": "c", "company": "Microsoft", "countOfStocks": 13},
    {"user": "d", "company": "Microsoft", "countOfStocks": 14},
    {"user": "e", "company": "Microsoft", "countOfStocks": 12}
  ]`;
  console.log(getPageData(dayTrade, 4, 3)); // page size = 3, page number = 2
 
